var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'

var express = require('express')
var path = require('path')
var app = express()
app.use('/', express.static(path.join(__dirname, 'public')))

app.listen(server_port, server_ip_address)
console.log('Listening to traffic')


